﻿using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Button;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Firebase.Auth;
using Firebase.Database;
using Java.Util;
using QuickServiceAdmin.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickServiceAdmin
{
    [Activity(Label = "AdminRegisterBusiness")]
    public class AdminRegisterBusiness : Activity, IOnSuccessListener, IOnFailureListener
    {
        private RadioButton R1, R2, R3, R4;
        FirebaseAuth auth;
        string key;
        private int month = System.DateTime.Now.Month;
        private int day = System.DateTime.Now.Day;
        private int year = System.DateTime.Now.Year;
        private TextInputEditText Name, Email, Phone, ComRegNo;
        private MaterialButton Create;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminRegisterBusiness);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            R1 = FindViewById<RadioButton>(Resource.Id.radioButton1);
            R2 = FindViewById<RadioButton>(Resource.Id.radioButton2);
            R3 = FindViewById<RadioButton>(Resource.Id.radioButton3);
            R4 = FindViewById<RadioButton>(Resource.Id.radioButton4);
            Name = FindViewById<TextInputEditText>(Resource.Id.Name);
            Email = FindViewById<TextInputEditText>(Resource.Id.Email);
            Phone = FindViewById<TextInputEditText>(Resource.Id.Phone);
            ComRegNo = FindViewById<TextInputEditText>(Resource.Id.ComRegNo);

            Create = FindViewById<MaterialButton>(Resource.Id.Create);
            Create.Click += Create_Click;
            
        }
        private void clean()
        {
            Name.Text = "";
            Email.Text = "";
            Phone.Text = "";
            ComRegNo.Text = "";
            
        }

        private bool validFields()
        {
            bool valid = true;
            if (string.IsNullOrEmpty(Name.Text))
            {
                Name.RequestFocus();
                valid = false;
                Name.Error = "This field cannot be empty";
            }

            if (string.IsNullOrEmpty(Email.Text) || !Email.Text.Contains('@'))
            {
                Email.RequestFocus();
                valid = false;
                Email.Error = "Email must be in correct format..with a @ symbol";
            }

            if (string.IsNullOrEmpty(Phone.Text) || Phone.Text.Length != 10)
            {
                Phone.RequestFocus();
                valid = false;
                Phone.Error = "Phone numbers must be 10 digits";
            }
            if (ComRegNo.Text == "")
            {
                ComRegNo.RequestFocus();
                valid = false;

                ComRegNo.Error = "Assign Company reg no.";
            }

            return valid;
        }
        private void register_User()
        {
            auth = new firebase_Helper().GetFirebaseAuth();
            auth.CreateUserWithEmailAndPassword(Email.Text.Trim().ToLower(), "123456")
                .AddOnFailureListener(this)
               .AddOnSuccessListener(this);
        }
        private void Create_Click(object sender, EventArgs e)
        {
            if (validFields() == true)
            {
                Toast.MakeText(this, "Atempting to make your registration... Please wait", ToastLength.Long).Show();
                register_User();
            }
        }

        private void create_BusinessAccount()
        {
            HashMap data = new HashMap();
            data.Put("company", Name.Text.Trim());
            data.Put("compMail", Email.Text.Trim().ToLower());
            data.Put("contacts", Phone.Text.Trim());
            data.Put("regno", ComRegNo.Text.Trim());
            data.Put("passwordCode", "123456");
            data.Put("DateRegistered", day.ToString() + '/' + month.ToString() + '/' + year.ToString());

            if (R1.Checked==true)
            {
                data.Put("profession", "Architecture");
                DatabaseReference databaseRef = firebase_Helper.GetDatabase().GetReference("Architecture").Child(auth.CurrentUser.Uid);
                databaseRef.SetValue(data); 
            }
            if (R2.Checked == true)
            {
                data.Put("profession", "Plumber");
                DatabaseReference databaseRef = firebase_Helper.GetDatabase().GetReference("Plumber").Child(auth.CurrentUser.Uid);
                databaseRef.SetValue(data);
            }
            if (R3.Checked == true)
            {
                data.Put("profession", "Carpenter");
                DatabaseReference databaseRef = firebase_Helper.GetDatabase().GetReference("Carpenter").Child(auth.CurrentUser.Uid);
                databaseRef.SetValue(data);

            }
            if (R4.Checked ==true)
            {
                data.Put("profession", "Electrician");
                DatabaseReference databaseRef = firebase_Helper.GetDatabase().GetReference("Electrician").Child(auth.CurrentUser.Uid);
                databaseRef.SetValue(data);
            }
            


            try
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.SetTitle("Notification");
                builder.SetIcon(Resource.Drawable.gmail);
                builder.SetMessage("write E-mail to " + Email.Text.Trim().ToLower());
                builder.SetNeutralButton("cancel", delegate
                {
                    clean();
                    builder.Dispose();
                    Toast.MakeText(this, "Successfully created account", ToastLength.Long).Show();
                    System.Threading.Thread.Sleep(2000);
                    Intent main = new Intent(this, typeof(MainActivity));
                    StartActivity(main);
                });
                builder.SetPositiveButton("mail", delegate
                {
                    var i = new Intent(Intent.ActionSend);
                    i.SetData(Android.Net.Uri.Parse("email"));
                    string[] s = { Email.Text.Trim().ToLower() };
                    i.PutExtra(Intent.ExtraEmail, s);
                    i.PutExtra(Intent.ExtraSubject, "Confirmation of account");
                    i.PutExtra(Intent.ExtraText, "Dear user Quick service SA has sucsessfully created your bussiness account. \nyour username is your email and password is 123456. do change the password for your convinience.\n\nRegards Quick Service SA");
                    i.SetType("message/rfc822");
                    Intent chooser = Intent.CreateChooser(i, " Launch your email app ");
                    StartActivity(chooser);
                    builder.Dispose();
                    clean();

                    Toast.MakeText(this, "Successfully created account", ToastLength.Long).Show();
                    /*System.Threading.Thread.Sleep(2000);
                    Intent main = new Intent(this, typeof(MainActivity));
                    StartActivity(main);*/
                });
                builder.Show();

            }
            catch (Exception)
            {

                Toast.MakeText(this, "badly formatted email or email app may not be installed on device", ToastLength.Long).Show();
            }
 
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle("warning");
            builder.SetIcon(Resource.Drawable.ic_error_outline_black_18dp);
            builder.SetMessage("Something went wrong: " + e.Message.ToString());
            builder.SetNeutralButton("OK", delegate
            {
                builder.Dispose();
            });
            builder.Show();
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            create_BusinessAccount();
        }
    }
}