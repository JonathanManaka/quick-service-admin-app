﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Firebase.Database;

namespace QuickServiceAdmin.Helpers
{
    class firebase_Helper
    {
        public static FirebaseDatabase GetDatabase()
        {
            FirebaseDatabase database;
            var app = FirebaseApp.InitializeApp(Application.Context);
            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApiKey("AIzaSyDON1G2svW26TuJgFflx5DjYst6pIMugag")
                    .SetApplicationId("assistme-555936")
                    .SetDatabaseUrl("https://assistme-55593.firebaseio.com")
                    .SetProjectId("assistme-55593")
                    .SetStorageBucket("assistme-55593.appspot.com")
                    .Build();
                app = FirebaseApp.InitializeApp(Application.Context, option);
                database = FirebaseDatabase.GetInstance(app);
                return database;

            }
            else
            {
                database = FirebaseDatabase.GetInstance(app);
                return database;

            }
        }
        public FirebaseAuth GetFirebaseAuth()
        {
            FirebaseAuth firebase;
            var app = FirebaseApp.InitializeApp(Application.Context);
            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApiKey("AIzaSyDON1G2svW26TuJgFflx5DjYst6pIMugag")
                    .SetApplicationId("assistme-555936")
                    .SetDatabaseUrl("https://assistme-55593.firebaseio.com")
                    .SetProjectId("assistme-55593")
                    .SetStorageBucket("assistme-55593.appspot.com")
                    .Build();
                app = FirebaseApp.InitializeApp(Application.Context, option);
                firebase = FirebaseAuth.GetInstance(app);
                return firebase;

            }
            else
            {

                firebase = FirebaseAuth.GetInstance(app);
                return firebase;
            }

        }

    }
}