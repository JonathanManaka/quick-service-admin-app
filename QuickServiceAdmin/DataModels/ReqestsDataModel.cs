﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickServiceAdmin.DataModels
{
    public class ReqestsDataModel
    {
        public string R_id { get; set; }
        public string CompanyName { get; set; }
        public string Regno { get; set; }
        public string status { get; set; }
        public string EmailComp { get; set; }
        public string specialComp { get; set; }
        public string dateDone { get; set; }
        public string invoiceLink { get; set; }
        public string custPhone { get; set; }
    }
}