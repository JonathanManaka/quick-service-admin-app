﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace QuickServiceAdmin.DataModels
{
    public class fixed_DataModel
    {
        public string Id_ { get; set; }
        public string f_contacts_ { get; set; }
        public string f_name_ { get; set; }
        public string f_passcode_ { get; set; }
        public string f_email { get; set; }
    }
}