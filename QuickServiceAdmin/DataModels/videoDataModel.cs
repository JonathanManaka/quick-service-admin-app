﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace QuickServiceAdmin.DataModels
{
    public class videoDataModel
    {
        public string IDvid { get; set; }
        public string LinkVideo { get; set; }
        public string nameperson { get; set; }
        public string Time { get; set; }
        public string caption { get; set; }
        public string userKey { get; set; }
    }
}