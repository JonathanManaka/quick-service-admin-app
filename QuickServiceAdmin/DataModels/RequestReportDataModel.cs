﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickServiceAdmin.DataModels
{
    public class RequestReportDataModel
    {
        public string _id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string company { get; set; }
        public string requstDate { get; set; }
        public string invoice { get; set; }

        public string phone { get; set; }
    }
}