﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace QuickServiceAdmin.DataModels
{
    public class Disosal_DataModel
    {
        public string id_dsposed { get; set; }
        public string comp_dis { get; set; }
        public string pass_dis { get; set; }
        public string proff_dis { get; set; }
        public string email_dis { get; set; }
        public string phon_dis { get; set; }
        public string date_dis { get; set; }
        public string reg_dis { get; set; }
    }
}