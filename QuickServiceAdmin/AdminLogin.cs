﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickServiceAdmin
{
    [Activity(Label = "GBV Admin",MainLauncher =true)]
    public class AdminLogin : Activity
    {
        private EditText passwordAdmin, Adminname;
        private Button loginAdmin;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminLogin);
            // Create your application here
            passwordAdmin = FindViewById<EditText>(Resource.Id.passwordAdmin);
            Adminname= FindViewById<EditText>(Resource.Id.Adminname);
            loginAdmin = FindViewById<Button>(Resource.Id.logAdmin);
            loginAdmin.Click += LoginAdmin_Click;
        }

        private void LoginAdmin_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(passwordAdmin.Text) && !string.IsNullOrWhiteSpace(Adminname.Text))
            {
                if(passwordAdmin.Text=="Q@dmin_S" && Adminname.Text == "Q_Admin")
                {
                    passwordAdmin.Text = "";
                    Intent intent = new Intent(this, typeof(MainActivity));
                    StartActivity(intent);
                }
                else
                {
                    Toast.MakeText(this, "Invalid input or wrong password", ToastLength.Long).Show();
                }

            }
            else
            {
                Toast.MakeText(this, "Invalid input or wrong password",ToastLength.Long).Show();
            }
        }
    }
}