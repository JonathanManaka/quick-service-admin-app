﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;
using QuickServiceAdmin.DataModels;
using System.Collections.Generic;
using Android.Graphics;

namespace QuickServiceAdmin.Adapters
{
    class RequstsAdapter : RecyclerView.Adapter
    {
        public event EventHandler<RequstsAdapterClickEventArgs> ItemClick;
        public event EventHandler<RequstsAdapterClickEventArgs> ItemLongClick;
        public event EventHandler<RequstsAdapterClickEventArgs> CancelClick;
        public event EventHandler<RequstsAdapterClickEventArgs> invoiceClick;
        public event EventHandler<RequstsAdapterClickEventArgs> callClick;
        List<ReqestsDataModel> items;

        public RequstsAdapter(List<ReqestsDataModel> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            var id = Resource.Layout.RequestsRow;
            itemView = LayoutInflater.From(parent.Context).
                   Inflate(id, parent, false);

            var vh = new RequstsAdapterViewHolder(itemView, OnClick, OnLongClick, OnCancelClick, OninvoiceClick, OncallClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        private int month = System.DateTime.Now.Month;
        private int day = System.DateTime.Now.Day;
        private int year = System.DateTime.Now.Year;
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as RequstsAdapterViewHolder;
            holder.CompanyName.Text = items[position].CompanyName;
            holder.Regno.Text = items[position].Regno;
            holder.status.Text = items[position].status;
            holder.EmailComp.Text = items[position].EmailComp;
            holder.specialComp.Text = items[position].specialComp;
            holder.custPhone.Text = items[position].custPhone;

            if (items[position].dateDone != day.ToString() + '/' + month.ToString() + '/' + year.ToString() || holder.status.Text != "awaiting approval")
            {
                holder.cancelR.Enabled = false;
                holder.cancelR.Visibility = ViewStates.Gone;
            }
            if (holder.status.Text== "awaiting approval")
            {
                holder.status.SetTextColor(Color.DeepSkyBlue);

            }
            if (holder.status.Text == "denied")
            {
                holder.status.SetTextColor(Color.DarkRed);

            }
            if (holder.status.Text == "approved")
            {
                holder.status.SetTextColor(Color.Green);
                holder.invoice.Visibility = ViewStates.Visible;
            }
            //set images
            if (holder.specialComp.Text == "Electricity")
            {
                holder.face.SetImageResource(Resource.Mipmap.electricianMan);
            }

            if (holder.specialComp.Text == "Plumbering")
            {
                holder.face.SetImageResource(Resource.Mipmap.plumber);
            }
            if (holder.specialComp.Text == "Carpentry")
            {
                holder.face.SetImageResource(Resource.Mipmap.carpenter);
            }
            if (holder.specialComp.Text == "Building"|| holder.specialComp.Text == "Architecture")
            {
                holder.face.SetImageResource(Resource.Mipmap.builder);
            }
        }
  

        public override int ItemCount => items.Count;

        void OnClick(RequstsAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(RequstsAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void OnCancelClick(RequstsAdapterClickEventArgs args) => CancelClick?.Invoke(this, args);
        void OninvoiceClick(RequstsAdapterClickEventArgs args) => invoiceClick?.Invoke(this, args);
        void OncallClick(RequstsAdapterClickEventArgs args) => callClick?.Invoke(this, args);
    }

    public class RequstsAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView CompanyName { get; set; }
        public TextView Regno { get; set; }
        public TextView status { get; set; }
        public TextView EmailComp { get; set; }
        public TextView specialComp { get; set; }
        public ImageView face { get; set; }
        public ImageView cancelR { get; set; }
        public ImageView invoice { get; set; }
        public TextView custPhone { get; set; }


        public RequstsAdapterViewHolder(View itemView, Action<RequstsAdapterClickEventArgs> clickListener,
                            Action<RequstsAdapterClickEventArgs> longClickListener, Action<RequstsAdapterClickEventArgs> CancelClickListener,
                            Action<RequstsAdapterClickEventArgs> invoiceClickListener,
                            Action<RequstsAdapterClickEventArgs> CallClickListener) : base(itemView)
        {
            cancelR= itemView.FindViewById<ImageView>(Resource.Id.cancelR);
            CompanyName = itemView.FindViewById<TextView>(Resource.Id.CompNam);
            Regno = itemView.FindViewById<TextView>(Resource.Id.RegistNo);
            EmailComp = itemView.FindViewById<TextView>(Resource.Id.emailComp);
            status = itemView.FindViewById<TextView>(Resource.Id.statusRequests);
            specialComp = itemView.FindViewById<TextView>(Resource.Id.specialize);
            face= itemView.FindViewById<ImageView>(Resource.Id.faceR);
            cancelR= itemView.FindViewById<ImageView>(Resource.Id.cancelR);
            invoice= itemView.FindViewById<ImageView>(Resource.Id.invoice);
            custPhone = itemView.FindViewById<TextView>(Resource.Id.companyPhone);
            cancelR.Click += (sender, e) => CancelClickListener(new RequstsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            invoice.Click += (sender, e) => invoiceClickListener(new RequstsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.Click += (sender, e) => clickListener(new RequstsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new RequstsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            custPhone.LongClick += (sender, e) => CallClickListener(new RequstsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class RequstsAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}