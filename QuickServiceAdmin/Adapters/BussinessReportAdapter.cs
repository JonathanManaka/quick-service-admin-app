﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;
using QuickServiceAdmin.DataModels;
using System.Collections.Generic;

namespace QuickServiceAdmin.Adapters
{
    class BussinessReportAdapter : RecyclerView.Adapter
    {
        public event EventHandler<BussinessReportAdapterClickEventArgs> ItemClick;
        public event EventHandler<BussinessReportAdapterClickEventArgs> ItemLongClick;
        public event EventHandler<BussinessReportAdapterClickEventArgs> DeleteReciptClick;
        List<RequestReportDataModel> items;

        public BussinessReportAdapter(List<RequestReportDataModel> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            var id = Resource.Layout.BusinessReportRow;
            itemView = LayoutInflater.From(parent.Context).
                   Inflate(id, parent, false);

            var vh = new BussinessReportAdapterViewHolder(itemView, OnClick, OnLongClick, OnDeleteReciptClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as BussinessReportAdapterViewHolder;
            //holder.TextView.Text = items[position];
            if (items[position].invoice!=" ")
            {
                if (items[position].invoice.Substring(0, 4) == "http")
                {
                    holder.btninvoice.Visibility = ViewStates.Visible;
                    holder.invoice.Text = "Yes";
                } 
            }
            holder.name.Text= items[position].name;
            holder.status.Text = items[position].status;
            holder.company.Text = items[position].company;
            holder.requstDate.Text = items[position].requstDate;
            holder.phone.Text = items[position].phone;
            
        }

        public override int ItemCount => items.Count;

        void OnClick(BussinessReportAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(BussinessReportAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void OnDeleteReciptClick(BussinessReportAdapterClickEventArgs args) => DeleteReciptClick?.Invoke(this, args);
    }

    public class BussinessReportAdapterViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }
        public TextView name { get; set; }
        public TextView status { get; set; }
        public TextView company { get; set; }
        public TextView requstDate { get; set; }
        public TextView invoice { get; set; }
        public ImageView btninvoice { get; set; }
        public TextView phone { get; set; }
        public ImageView ReceptCancel { get; set; }
        public BussinessReportAdapterViewHolder(View itemView, Action<BussinessReportAdapterClickEventArgs> clickListener,
                            Action<BussinessReportAdapterClickEventArgs> longClickListener, Action<BussinessReportAdapterClickEventArgs> DeleteReceiptClickListener) : base(itemView)
        {
            name = itemView.FindViewById<TextView>(Resource.Id.custName);
            company = itemView.FindViewById<TextView>(Resource.Id.CompNam);
            requstDate = itemView.FindViewById<TextView>(Resource.Id.ReqComp);
            status = itemView.FindViewById<TextView>(Resource.Id.statusRequests);
            invoice = itemView.FindViewById<TextView>(Resource.Id.isInvoice);
            phone = itemView.FindViewById<TextView>(Resource.Id.custPhone);
            btninvoice= itemView.FindViewById<ImageView>(Resource.Id.btninvoice);
            ReceptCancel= itemView.FindViewById<ImageView>(Resource.Id.ReceptCancel);
            itemView.Click += (sender, e) => clickListener(new BussinessReportAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new BussinessReportAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            ReceptCancel.Click += (sender, e) => DeleteReceiptClickListener(new BussinessReportAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class BussinessReportAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}