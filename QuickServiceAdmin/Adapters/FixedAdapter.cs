﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using QuickServiceAdmin.DataModels;

namespace QuickServiceAdmin.Adapters
{
    class FixedAdapter : RecyclerView.Adapter
    {
        public event EventHandler<FixedAdapterClickEventArgs> ItemClick;
        public event EventHandler<FixedAdapterClickEventArgs> ItemLongClick;
        public event EventHandler<FixedAdapterClickEventArgs> deltClick;
        List<fixed_DataModel> items;

        public FixedAdapter(List<fixed_DataModel> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            var id = Resource.Layout.standardRow;
            itemView = LayoutInflater.From(parent.Context).
                   Inflate(id, parent, false);

            var vh = new FixedAdapterViewHolder(itemView, OnClick, OnLongClick, OndeltClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as FixedAdapterViewHolder;
            holder.txtname.Text = items[position].f_name_;
            holder.txtphone.Text = items[position].f_contacts_;
        }

        public override int ItemCount => items.Count;

        void OnClick(FixedAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(FixedAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void OndeltClick(FixedAdapterClickEventArgs args) => deltClick.Invoke(this, args);
    }

    public class FixedAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView txtname { get; set; }
        public TextView txtphone { get; set; }
        public ImageView delt { get; set; }

        public FixedAdapterViewHolder(View itemView, Action<FixedAdapterClickEventArgs> clickListener,
                            Action<FixedAdapterClickEventArgs> longClickListener, Action<FixedAdapterClickEventArgs> deltListener) : base(itemView)
        {
            txtname = itemView.FindViewById<TextView>(Resource.Id.txtname);
            txtphone = itemView.FindViewById<TextView>(Resource.Id.txtphone);
            delt= itemView.FindViewById<ImageView>(Resource.Id.delt);


            itemView.Click += (sender, e) => clickListener(new FixedAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new FixedAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            delt.Click += (sender, e) => deltListener(new FixedAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class FixedAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}