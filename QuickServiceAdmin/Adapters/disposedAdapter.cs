﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using QuickServiceAdmin.DataModels;

namespace QuickServiceAdmin.Adapters
{
    class disposedAdapter : RecyclerView.Adapter
    {
        public event EventHandler<disposedAdapterClickEventArgs> ItemClick;
        public event EventHandler<disposedAdapterClickEventArgs> ItemLongClick;
        public event EventHandler<disposedAdapterClickEventArgs> deltClick;
        List<Disosal_DataModel> items;

        public disposedAdapter(List<Disosal_DataModel> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            var id = Resource.Layout.bussinessRow;
            itemView = LayoutInflater.From(parent.Context).
                   Inflate(id, parent, false);

            var vh = new disposedAdapterViewHolder(itemView, OnClick, OnLongClick, OndeltClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as disposedAdapterViewHolder;
            holder.txtcomp.Text = items[position].comp_dis;
            holder.txtprof.Text = items[position].proff_dis;
        }

        public override int ItemCount => items.Count;

        void OnClick(disposedAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(disposedAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void OndeltClick(disposedAdapterClickEventArgs args) => deltClick.Invoke(this, args);
    }

    public class disposedAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView txtcomp { get; set; }
        public TextView txtprof { get; set; }
        public ImageView delted { get; set; }


        public disposedAdapterViewHolder(View itemView, Action<disposedAdapterClickEventArgs> clickListener,
                            Action<disposedAdapterClickEventArgs> longClickListener,
                            Action<disposedAdapterClickEventArgs> deltListener) : base(itemView)
        {
            txtcomp = itemView.FindViewById<TextView>(Resource.Id.txtcomp);
            txtprof = itemView.FindViewById<TextView>(Resource.Id.txtproff);
            delted = itemView.FindViewById<ImageView>(Resource.Id.delted);

            itemView.Click += (sender, e) => clickListener(new disposedAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new disposedAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            delted.Click += (sender, e) => deltListener(new disposedAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class disposedAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}