﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using QuickServiceAdmin.DataModels;
using System.Collections.Generic;
using static Android.Media.MediaPlayer;
using Android.Media;
using Android.Runtime;
using System.Threading.Tasks;

namespace QuickServiceAdmin.Adapters
{
    class adminvideo : RecyclerView.Adapter, IOnCompletionListener, IOnPreparedListener, IOnErrorListener
    {
        public event EventHandler<adminvideoClickEventArgs> ItemClick;
        public event EventHandler<adminvideoClickEventArgs> ItemLongClick;
        public event EventHandler<adminvideoClickEventArgs> PlayClick;
        public event EventHandler<adminvideoClickEventArgs> deltClick;
        List<videoDataModel> items;
        string User_Key = "";

        public adminvideo(List<videoDataModel> data,string keyserch)
        {
            items = data;
            User_Key = keyserch;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            var id = Resource.Layout.videoRow;
            itemView = LayoutInflater.From(parent.Context).
                   Inflate(id, parent, false);

            var vh = new adminvideoViewHolder(itemView, OnClick, OnLongClick, OnPlayClick, OndeltClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var holder = viewHolder as adminvideoViewHolder;
            holder.nameOwner.Text = items[position].nameperson;
            holder.Time.Text = items[position].Time;
            holder.caption.Text= items[position].caption;
     
            getimage(items[position].LinkVideo, holder.Video_View);
        }
        void getimage(string Url, VideoView vidView)
        {
            vidView.SetVideoURI(Android.Net.Uri.Parse(Url));
            vidView.SetOnCompletionListener(this);
            vidView.SetOnPreparedListener(this);
            vidView.SetOnErrorListener(this);
            vidView.Pause();
            //vidView.SeekTo(1000);

        }
        public void OnCompletion(MediaPlayer mp)
        {
            mp.Stop();
        }
        public void OnPrepared(MediaPlayer mp)
        {
            try
            {
                mp.Start();
                mp.SetVolume(0, 0);
                
            }
            catch (Exception)
            {
                
            }

        }
        public bool OnError(MediaPlayer mp, [GeneratedEnum] MediaError what, int extra)
        {
            
            return true;
        }

        public override int ItemCount => items.Count;

        void OnClick(adminvideoClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(adminvideoClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void OnPlayClick(adminvideoClickEventArgs args) => PlayClick.Invoke(this, args);
        void OndeltClick(adminvideoClickEventArgs args) => deltClick.Invoke(this, args);
    }

    public class adminvideoViewHolder : RecyclerView.ViewHolder
    {
        public TextView Time { get; set; }
        public VideoView Video_View { get; set; }
        public TextView nameOwner { get; set; }
        public ImageView play { get; set; }
        public TextView caption { get; set; }
        public ImageView delt { get; set; }


        public adminvideoViewHolder(View itemView, Action<adminvideoClickEventArgs> clickListener,
                            Action<adminvideoClickEventArgs> longClickListener, Action<adminvideoClickEventArgs> PlayListener,
                            Action<adminvideoClickEventArgs> deltListener) : base(itemView)
        {
            Video_View = itemView.FindViewById<VideoView>(Resource.Id.PlayVideo);
            nameOwner = itemView.FindViewById<TextView>(Resource.Id.nameOwner);
            Time = itemView.FindViewById<TextView>(Resource.Id.time);
            caption = itemView.FindViewById<TextView>(Resource.Id.caption_);
            play= itemView.FindViewById<ImageView>(Resource.Id.play);

            delt = itemView.FindViewById<ImageView>(Resource.Id.deletefile);

            itemView.Click += (sender, e) => clickListener(new adminvideoClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new adminvideoClickEventArgs { View = itemView, Position = AdapterPosition });
            Video_View.Clickable = false;
            play.Click += (sender, e) => PlayListener(new adminvideoClickEventArgs { View = itemView, Position = AdapterPosition });
            delt.Click += (sender, e) => deltListener(new adminvideoClickEventArgs { View = itemView, Position = AdapterPosition });
            
        }
    }

    public class adminvideoClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}