﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using QuickServiceAdmin.Adapters;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.EventListeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickServiceAdmin
{
    [Activity(Label = "standardRequests")]
    public class standardRequests : Activity
    {
        RequstsAdapter messageAdapter;
        private RecyclerView RequestsRecycler;
        string Id;
        private List<ReqestsDataModel> offList = new List<ReqestsDataModel>();
        private EventRequests offData = new EventRequests();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.standardRequest);
            RequestsRecycler = FindViewById<RecyclerView>(Resource.Id.requestsRecycler);
            String sumString = Intent.GetStringExtra("key");
            Id = sumString;
            fetchMessages();
            // Create your application here
        }

        private void fetchMessages()
        {
            offData.Retriverequests(Id);
            offData.RetrivedMessages += OffData_RetrivedMessages;

        }

        private void OffData_RetrivedMessages(object sender, EventRequests.RetrieveMessageEventHandeler e)
        {
            offList = e.messagesList;
            setupRecycler();
        }
        private void AutoRefresh()
        {
            offList.Clear();
            messageAdapter.NotifyDataSetChanged();
            fetchMessages();
        }
        private void setupRecycler()
        {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            messageAdapter = new RequstsAdapter(offList);
            RequestsRecycler.SetLayoutManager(linearLayoutManager);
            RequestsRecycler.SetAdapter(messageAdapter);
            messageAdapter.CancelClick += MessageAdapter_CancelClick;
            
            messageAdapter.callClick += MessageAdapter_callClick;
        }
        private void MessageAdapter_CancelClick(object sender, RequstsAdapterClickEventArgs e)
        {

            try
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                builder.SetTitle("Confirm cancelation");
                builder.SetMessage("This cannot be undone, Are you sure ?...");
                builder.SetIcon(Resource.Mipmap.authorizeddealer);
                builder.SetNeutralButton("dont cancel", delegate
                {
                    builder.Dispose();
                });
                builder.SetPositiveButton("yes cancel", delegate
                {
                    Helpers.firebase_Helper.GetDatabase().GetReference("Requests").Child(Id).Child(offList[e.Position].R_id).RemoveValue();

                    Helpers.firebase_Helper.GetDatabase().GetReference("BusinessCopyRequests").Child(offList[e.Position].R_id).Child(Id).RemoveValue();

                    Toast.MakeText(this, "successfully...canceled your request", ToastLength.Long).Show();
                    AutoRefresh();
                    builder.Dispose();
                });
                builder.Show();

            }
            catch (System.Exception r)
            {
                Toast.MakeText(this, "Something went wrong: " + r.Message, ToastLength.Long).Show();
            }
        }
        private void MessageAdapter_callClick(object sender, RequstsAdapterClickEventArgs e)
        {
            try
            {
                if (offList[e.Position].custPhone.Trim().Any(char.IsLetter) == true)
                {
                    Toast.MakeText(this, "invalid phone numbers detected", ToastLength.Long).Show();
                }
                else
                {
                    Intent i = new Intent(Intent.ActionDial, Android.Net.Uri.Parse("tel:" + offList[e.Position].custPhone.Trim()));
                    StartActivity(i);
                }

            }
            catch (Exception)
            {

                Toast.MakeText(this, "Phone numbers not in correct format or reference is empty", ToastLength.Long).Show();
            }
        }
    }
}