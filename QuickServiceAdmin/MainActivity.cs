﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using QuickServiceAdmin.Fragments;
using System.Collections.Generic;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using SupportFragment = Android.Support.V4.App;

namespace QuickServiceAdmin
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = false)]
    public class MainActivity : Android.Support.V4.App.FragmentActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        TextView textMessage;
        private SupportFragment.Fragment Currentfragment;
        private Fragmentprofile fragmentprofile;
        private FragmentRequests fragmentRequests;
        private FragmentMarket fragmentMarket;
        private Stack<SupportFragment.Fragment> mstackFragment;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            textMessage = FindViewById<TextView>(Resource.Id.message);
            textMessage.Text = "";
            BottomNavigationView navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);


            fragmentMarket = new FragmentMarket();
            fragmentprofile = new Fragmentprofile();
            fragmentRequests = new FragmentRequests();
            mstackFragment = new Stack<SupportFragment.Fragment>();
            // Create your application here
            var trans = SupportFragmentManager.BeginTransaction();
            trans.Add(Resource.Id.fragmentContainer, fragmentRequests, "Fragment3");
            trans.Hide(fragmentRequests);
            trans.Add(Resource.Id.fragmentContainer, fragmentMarket, "Fragment2");
            trans.Hide(fragmentMarket);
            trans.Add(Resource.Id.fragmentContainer, fragmentprofile, "Fragment1");
            trans.Commit();

            Currentfragment = fragmentprofile;
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        private void ShowFragment(SupportFragment.Fragment fragment)
        {
            var trans = SupportFragmentManager.BeginTransaction();
            trans.Hide(Currentfragment);
            trans.Show(fragment);
            trans.AddToBackStack(null);
            trans.Commit();
            mstackFragment.Push(Currentfragment);
            Currentfragment = fragment;
       
        }
        public override void OnBackPressed()
        {
            /*if(SupportFragmentManager.BackStackEntryCount>0)
            {
                SupportFragmentManager.PopBackStack();
                Currentfragment = mstackFragment.Pop();
                SetFocus();
            }
            else
            {
                base.OnBackPressed();
            }*/

        }
        public bool OnNavigationItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.navigation_home:
                    textMessage.Text="";
                    ShowFragment(fragmentprofile);
                    return true;
                case Resource.Id.navigation_dashboard:
                    textMessage.Text = "";
                    ShowFragment(fragmentMarket);
                    return true;
                case Resource.Id.navigation_notifications:
                    textMessage.Text = "";
                    ShowFragment(fragmentRequests);
                    return true;
                case Resource.Id.navigation_poweroff:
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.SetTitle("log-out");
                    builder.SetIcon(Resource.Drawable.ic_power_settings_new_black_18dp);
                    builder.SetMessage("Continue to sign-out..?");
                    builder.SetPositiveButton("Yes", delegate
                    {
                        Finish();
                        builder.Dispose();
                    });
                    builder.SetNeutralButton("no", delegate
                    {
                        builder.Dispose();
                    });
                    builder.Show();

                    return true;
            }
            return false;
        }
    }
}

