﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using QuickServiceAdmin.Adapters;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.EventListeners;
using QuickServiceAdmin.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickServiceAdmin
{
    [Activity(Label = "BusinessRequests")]
    public class BusinessRequests : Activity
    {
        private List<RequestReportDataModel> fixedList = new List<RequestReportDataModel>();
        private EventReport fixedData = new EventReport();
        private BussinessReportAdapter fixedAdapter;
        private RecyclerView bussinesrecycler;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BusinessReport);
            bussinesrecycler = FindViewById<RecyclerView>(Resource.Id.recyclerView1);
            Retrieve_fixed();

        }
        private void Retrieve_fixed()
        {
            fixedData.RetriveReport(Intent.GetStringExtra("BussKey"));
            fixedData.RetrivedMessages += FixedData_RetrivedMessages;
        }

        private void FixedData_RetrivedMessages(object sender, EventReport.RetrieveMessageEventHandeler e)
        {
            fixedList = e.messagesList;
            setupRecyclerFixed();
        }

        private void setupRecyclerFixed()
        {
            LinearLayoutManager linearLayoutManagerfixed = new LinearLayoutManager(this);
            fixedAdapter = new BussinessReportAdapter(fixedList);
            bussinesrecycler.SetLayoutManager(linearLayoutManagerfixed);
            bussinesrecycler.SetAdapter(fixedAdapter);
            fixedAdapter.DeleteReciptClick += FixedAdapter_DeleteReciptClick;
        }

        private void FixedAdapter_DeleteReciptClick(object sender, BussinessReportAdapterClickEventArgs e)
        {
            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
            builder.SetTitle("Warning");
            builder.SetIcon(Resource.Drawable.ic_auto_delete_black_18dp);
            builder.SetMessage("You are about to Delete this receipt from the System(Database)");
            builder.SetPositiveButton("Delete", delegate
            {
                
                DatabaseReference database;
                database = firebase_Helper.GetDatabase().GetReference("BusinessCopyRequests").Child(Intent.GetStringExtra("BussKey"));
                database.Child(fixedList[e.Position]._id).RemoveValue();
                builder.Dispose();

                fixedList.Clear();
                fixedAdapter.NotifyDataSetChanged();
                Retrieve_fixed();

                Toast.MakeText(this, "receipt removed successfully", ToastLength.Long).Show();


            });
            builder.SetNeutralButton("Cancel", delegate
            {
                builder.Dispose();
            });
            builder.Show();
        }
    }
}