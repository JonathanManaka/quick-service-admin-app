﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using Android.Support.V4.App;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.V7.Widget;
using QuickServiceAdmin.Adapters;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.EventListeners;
using Firebase.Database;
using QuickServiceAdmin.Helpers;

namespace QuickServiceAdmin.Fragments
{
    public class FragmentMarket : Android.Support.V4.App.Fragment
    {
        private ImageView Add_Advert;
        private RecyclerView TimeRecycler;
        adminvideo T_Adapter;
        string OwnerID="AAAAAAAAAAAAAAAAAAAAAAAA";
        private List<videoDataModel> listmodel = new List<videoDataModel>();
        private adminVidData evenObj = new adminVidData();
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.FragmentMarket, container, false);
            TimeRecycler = view.FindViewById<RecyclerView>(Resource.Id.ImaagesRecyler);
            Retrieve_Data();
            return view;
        }

        private void setupRecycler()
        {
            try
            {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Activity);

                T_Adapter = new adminvideo(listmodel, OwnerID);
                TimeRecycler.SetLayoutManager(linearLayoutManager);
                TimeRecycler.SetAdapter(T_Adapter);
                T_Adapter.deltClick += T_Adapter_deltClick;
                T_Adapter.PlayClick += T_Adapter_PlayClick;
            }
            catch (Exception b)
            {
                Toast.MakeText(Activity, b.Message, ToastLength.Long).Show();

            }

        }

        private void T_Adapter_deltClick(object sender, adminvideoClickEventArgs e)
        {
            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(Activity);
            builder.SetTitle("Warning");
            builder.SetIcon(Resource.Drawable.ic_auto_delete_black_18dp);
            builder.SetMessage("You are about to Delete your video from Quick Service market");
            builder.SetPositiveButton("Delete", delegate
            {

                DatabaseReference database;
                database = firebase_Helper.GetDatabase().GetReference("BusinessAdverts");
                database.Child(listmodel[e.Position].IDvid).RemoveValue();
                builder.Dispose();


                Toast.MakeText(Activity, "file successfully removed", ToastLength.Long).Show();
                Intent valt = new Intent(Activity, typeof(MainActivity));
                StartActivity(valt);

            });
            builder.SetNeutralButton("Cancel", delegate
            {
                builder.Dispose();
            });
            builder.Show();
        }

        private void T_Adapter_PlayClick(object sender, adminvideoClickEventArgs e)
        {
            /*Intent i = new Intent(Activity, typeof(PlayVideo));
            i.PutExtra("url", listmodel[e.Position].LinkVideo);
            StartActivity(i);*/
        }
        private void AutoRefresh()
        {
            //offList.Clear();
            T_Adapter.NotifyDataSetChanged();
            //Retrieve_Data();
        }
        public void Retrieve_Data()
        {
            evenObj.RetriveMessage();
            evenObj.RetrivedMessages += EvenObj_RetrivedMessages;
        }

        private void EvenObj_RetrivedMessages(object sender, adminVidData.RetrieveMessageEventHandeler e)
        {
            listmodel = e.messagesList;
            setupRecycler();
        }
    }
}