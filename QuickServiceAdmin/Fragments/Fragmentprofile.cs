﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content.PM;
using Xamarin.Essentials;
using System.Text;
using Android.Support.Design.Button;
using Android.Support.V7.Widget;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.EventListeners;
using QuickServiceAdmin.Adapters;
using Android.Graphics;
using Firebase.Auth;
using Firebase.Database;
using QuickServiceAdmin.Helpers;
using Android.Gms.Tasks;

namespace QuickServiceAdmin.Fragments
{
    public class Fragmentprofile : Android.Support.V4.App.Fragment, IOnSuccessListener, IOnFailureListener
    {
        private Android.App.AlertDialog dialog, dialogdetails;
        private Android.App.AlertDialog.Builder dialogBuilder;
        private MaterialButton standard, business, updateCompany;
        FirebaseAuth auth;
        private TextView txtCaption;
        private RadioButton R1, R2, R3, R4;
        private RecyclerView bussinesrecycler, standardrecycler;
        private ImageView electrician, plumber, carpenter, builder;
        private ImageView addpeople;
        private DisposalEvent dispData = new DisposalEvent();
        private List<Disosal_DataModel> disposeList = new List<Disosal_DataModel>();
        private disposedAdapter disposed_Adapter;
        TextView txt2, txt6;
        private EditText txt1 , txt3, txt4, txt5;
        private List<fixed_DataModel> fixedList = new List<fixed_DataModel>();
        private fixedEvent fixedData = new fixedEvent();
        private FixedAdapter fixedAdapter;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            View view = inflater.Inflate(Resource.Layout.fragmentprofiles, container, false);
            addpeople= view.FindViewById<ImageView>(Resource.Id.addpeople);
            standard = view.FindViewById<MaterialButton>(Resource.Id.standard);
            business = view.FindViewById<MaterialButton>(Resource.Id.business);
            bussinesrecycler = view.FindViewById<RecyclerView>(Resource.Id.bussinesrecycler);
            standardrecycler = view.FindViewById<RecyclerView>(Resource.Id.standardrecycler);
            txtCaption= view.FindViewById<TextView>(Resource.Id.txtCaption);
            standard.Click += Standard_Click;
            business.Click += Business_Click;
            addpeople.Click += Addpeople_Click;
            standard.PerformClick();
            Retrieve_fixed();
            return view;
        }

        private void Addpeople_Click(object sender, EventArgs e)
        {
            Intent Student = new Intent(Activity, typeof(QuickServiceAdmin.AdminRegisterBusiness));
            StartActivity(Student);
        }

        private void Business_Click(object sender, EventArgs e)
        {
            txtCaption.Text = "business accounts";
            standardrecycler.Visibility = ViewStates.Gone;
            bussinesrecycler.Visibility = ViewStates.Visible;

            try
            {
                dialogBuilder = new Android.App.AlertDialog.Builder(Activity);
                LayoutInflater inflater = LayoutInflater.From(Activity);
                View dialogView = inflater.Inflate(Resource.Layout.chooseAccount, null);
                electrician = dialogView.FindViewById<ImageView>(Resource.Id.electrician);
                plumber = dialogView.FindViewById<ImageView>(Resource.Id.plumber);
                carpenter = dialogView.FindViewById<ImageView>(Resource.Id.carpenter);
                builder = dialogView.FindViewById<ImageView>(Resource.Id.builder);
                electrician.Click += Electrician_Click;
                plumber.Click += Plumber_Click;
                carpenter.Click += Carpenter_Click;
                builder.Click += Builder_Click;
                dialogBuilder.SetView(dialogView);
                dialogBuilder.SetCancelable(true);
                dialog = dialogBuilder.Create();
                dialog.Show();

            }
            catch (System.Exception)
            {

            }
        }

        private void Builder_Click(object sender, EventArgs e)
        {
            table = "Architecture";
            Retrieve_dispose("Architecture");
            txtCaption.Text = "";
            if(disposed_Adapter!=null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }
        string table = "";
        private void Carpenter_Click(object sender, EventArgs e)
        {
            table = "Carpenter";
            Retrieve_dispose("Carpenter");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }

        private void Plumber_Click(object sender, EventArgs e)
        {
            table = "Plumber";
            Retrieve_dispose("Plumber");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }

        private void Electrician_Click(object sender, EventArgs e)
        {
            table = "Electrician";
            Retrieve_dispose("Electrician");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }
        private void setUpRecyclerDisposal()
        {
            LinearLayoutManager linearLayoutManagerDisposal = new LinearLayoutManager(Activity);
            disposed_Adapter = new disposedAdapter(disposeList);
            bussinesrecycler.SetLayoutManager(linearLayoutManagerDisposal);
            bussinesrecycler.SetAdapter(disposed_Adapter);
            disposed_Adapter.ItemClick += Disposed_Adapter_ItemClick;
            disposed_Adapter.deltClick += Disposed_Adapter_deltClick;
        }

        private void Disposed_Adapter_deltClick(object sender, disposedAdapterClickEventArgs e)
        {
            string pass, email;
            pass = disposeList[e.Position].pass_dis;
            email = disposeList[e.Position].email_dis;
            Id = disposeList[e.Position].id_dsposed;
            auth = new firebase_Helper().GetFirebaseAuth();
            auth.SignInWithEmailAndPassword(email.Trim().ToLower(), pass)
                .AddOnFailureListener(this)
               .AddOnSuccessListener(this);
        }

        private void Disposed_Adapter_ItemClick(object sender, disposedAdapterClickEventArgs e)
        {
            try
            {
                Id= disposeList[e.Position].id_dsposed;
                dialogBuilder = new Android.App.AlertDialog.Builder(Activity);
                LayoutInflater inflater = LayoutInflater.From(Activity);
                View dialogView = inflater.Inflate(Resource.Layout.fulldetails, null);
                txt1 = dialogView.FindViewById<EditText>(Resource.Id.txt1);
                txt2 = dialogView.FindViewById<TextView>(Resource.Id.txt2);
                txt3 = dialogView.FindViewById<EditText>(Resource.Id.txt3);
                txt4 = dialogView.FindViewById<EditText>(Resource.Id.txt4);
                txt5 = dialogView.FindViewById<EditText>(Resource.Id.txt5);
                txt6 = dialogView.FindViewById<TextView>(Resource.Id.txt6);
                R1 = dialogView.FindViewById<RadioButton>(Resource.Id.radioButton1);
                R2 = dialogView.FindViewById<RadioButton>(Resource.Id.radioButton2);
                R3 = dialogView.FindViewById<RadioButton>(Resource.Id.radioButton3);
                R4 = dialogView.FindViewById<RadioButton>(Resource.Id.radioButton4);
                updateCompany = dialogView.FindViewById<MaterialButton>(Resource.Id.updateCompany);
                updateCompany.Click += UpdateCompany_Click;
                dialogBuilder.SetView(dialogView);
                dialogBuilder.SetCancelable(true);
                dialogdetails = dialogBuilder.Create();
                dialogdetails.Show();

                txt1.SetTextColor(Color.Red);
                txt1.Text = disposeList[e.Position].comp_dis;
                txt2.Text = disposeList[e.Position].email_dis;
                txt3.Text =  disposeList[e.Position].proff_dis;
                txt4.Text = disposeList[e.Position].phon_dis;
                txt5.Text = disposeList[e.Position].reg_dis;
                txt6.Text = disposeList[e.Position].date_dis;

            }
            catch (System.Exception)
            {

            }
        }

        private void UpdateCompany_Click(object sender, EventArgs e)
        {
            try
            {
                firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("company").SetValue(txt1.Text);
                
               // firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("profession").SetValue(txt3.Text);
                /*if (R1.Checked == true)
                {
                    firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("profession").SetValue(R1.Text);
                }
                if (R2.Checked == true)
                {
                    firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("profession").SetValue(R2.Text);
                }
                if (R3.Checked == true)
                {
                    firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("profession").SetValue(R3.Text);

                }
                if (R4.Checked == true)
                {
                    firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("profession").SetValue(R1.Text);
                }*/
                firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("contacts").SetValue(txt4.Text);
                firebase_Helper.GetDatabase().GetReference(table).Child(Id).Child("regno").SetValue(txt5.Text);
                AlertDialog.Builder builder = new AlertDialog.Builder(Activity);
                builder.SetTitle("notification");
                builder.SetIcon(Resource.Mipmap.authorizeddealer);
                builder.SetMessage("successfully updated");
                builder.SetPositiveButton("ok", delegate
                {
                    builder.Dispose();
                    dialogdetails.Dismiss();
                });
                builder.Show();

            }
            catch (System.Exception er)
            {
                Toast.MakeText(Activity, "error: " + er.Message.ToString(), ToastLength.Long).Show();
            }
        }

        private void Retrieve_dispose(string n)
        {
            dispData.RetriveDispose(n);
            dispData.RetrivedMessages += DispData_RetrivedMessages;
        }
        private void DispData_RetrivedMessages(object sender, DisposalEvent.RetrieveMessageEventHandeler e)
        {
            disposeList = e.messagesList;
            setUpRecyclerDisposal();
            
        }
        private void Standard_Click(object sender, EventArgs e)
        {
            txtCaption.Text = "standard accounts";
            standardrecycler.Visibility = ViewStates.Visible;
            bussinesrecycler.Visibility= ViewStates.Gone;

        }

        private void Retrieve_fixed()
        {
            fixedData.RetriveFixed();
            fixedData.RetrivedMessages += FixedData_RetrivedMessages;
        }
        private void setupRecyclerFixed()
        {
            LinearLayoutManager linearLayoutManagerfixed = new LinearLayoutManager(Activity);
            fixedAdapter = new FixedAdapter(fixedList);
            standardrecycler.SetLayoutManager(linearLayoutManagerfixed);
            standardrecycler.SetAdapter(fixedAdapter);
            fixedAdapter.deltClick += FixedAdapter_deltClick;


        }
        private void deleteAccount()
        {
            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(Activity);
            builder.SetTitle("Warning");
            builder.SetIcon(Resource.Drawable.ic_auto_delete_black_18dp);
            builder.SetMessage("You are about to Delete your account from the System(Database)");
            builder.SetPositiveButton("Delete", delegate
            {
                auth.CurrentUser.Delete();
                DatabaseReference database;
                database = firebase_Helper.GetDatabase().GetReference(table);
                database.Child(Id).RemoveValue();
                builder.Dispose();


                Toast.MakeText(Activity, "user removed successfully", ToastLength.Long).Show();
                

            });
            builder.SetNeutralButton("Cancel", delegate
            {
                builder.Dispose();
            });
            builder.Show();
        }
        string Id = "";
        private void FixedAdapter_deltClick(object sender, FixedAdapterClickEventArgs e)
        {
            string pass,email;
            table = "standardUsers";
            pass = fixedList[e.Position].f_passcode_;
            email= fixedList[e.Position].f_email;
            Id= fixedList[e.Position].Id_;
            auth = new firebase_Helper().GetFirebaseAuth();
            auth.SignInWithEmailAndPassword(email.Trim().ToLower(), pass)
                .AddOnFailureListener(this)
               .AddOnSuccessListener(this);
        }

        private void FixedData_RetrivedMessages(object sender, fixedEvent.RetrieveMessageEventHandeler e)
        {
            fixedList = e.messagesList;
            setupRecyclerFixed();
        }
        private void AutoRefresh()
        {
            fixedList.Clear();
            fixedAdapter.NotifyDataSetChanged();
            Retrieve_fixed();
            
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            deleteAccount();
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            Toast.MakeText(Activity, e.Message, ToastLength.Long).Show();
        }
    }
}