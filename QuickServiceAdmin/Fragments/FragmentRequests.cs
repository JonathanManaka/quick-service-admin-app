﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using Android.Support.V4.App;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.Design.Button;
using Android.Support.V7.Widget;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.Adapters;
using QuickServiceAdmin.EventListeners;

namespace QuickServiceAdmin.Fragments
{
    public class FragmentRequests : Android.Support.V4.App.Fragment
    {
        private RecyclerView bussinesrecycler, standardrecycler;
        private MaterialButton Standard, Business;
        private TextView txtCaption;
        private Android.App.AlertDialog dialog, dialogdetails;
        private Android.App.AlertDialog.Builder dialogBuilder;
        private ImageView electrician, plumber, carpenter, builder;
        private ImageView addpeople;
        private DisposalEvent dispData = new DisposalEvent();
        private List<Disosal_DataModel> disposeList = new List<Disosal_DataModel>();
        private QuickServiceAdmin.ExtraAdapter.disposedAdapter disposed_Adapter;
        private List<fixed_DataModel> fixedList = new List<fixed_DataModel>();
        private fixedEvent fixedData = new fixedEvent();
        private QuickServiceAdmin.ExtraAdapter.FixedAdapter fixedAdapter;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.FragmentRequestsUI, container, false);
            Standard = view.FindViewById<MaterialButton>(Resource.Id.materialButton1);
            Business = view.FindViewById<MaterialButton>(Resource.Id.materialButton2);
            Business.Click += Business_Click;
            bussinesrecycler = view.FindViewById<RecyclerView>(Resource.Id.requestsBussiness);
            standardrecycler = view.FindViewById<RecyclerView>(Resource.Id.requestsStandard);
            txtCaption = view.FindViewById<TextView>(Resource.Id.textView1);
            Standard.Click += Standard_Click;
            Standard.PerformClick();
            return view;
        }

       

        private void Standard_Click(object sender, EventArgs e)
        {
            txtCaption.Text = "standard accounts";
            standardrecycler.Visibility = ViewStates.Visible;
            bussinesrecycler.Visibility = ViewStates.Gone;
            Retrieve_fixed();
        }

        private void Retrieve_fixed()
        {
            fixedData.RetriveFixed();
            fixedData.RetrivedMessages += FixedData_RetrivedMessages;
        }
        private void setupRecyclerFixed()
        {
            LinearLayoutManager linearLayoutManagerfixed = new LinearLayoutManager(Activity);
            fixedAdapter = new QuickServiceAdmin.ExtraAdapter.FixedAdapter(fixedList);
            standardrecycler.SetLayoutManager(linearLayoutManagerfixed);
            standardrecycler.SetAdapter(fixedAdapter);
            fixedAdapter.ItemClick += FixedAdapter_ItemClick; 

        }

        private void FixedAdapter_ItemClick(object sender, ExtraAdapter.FixedAdapterClickEventArgs e)
        {
            Intent i = new Intent(Activity, typeof(standardRequests));
            i.PutExtra("key", fixedList[e.Position].Id_);
            StartActivity(i);
        }

        private void FixedData_RetrivedMessages(object sender, fixedEvent.RetrieveMessageEventHandeler e)
        {
            fixedList = e.messagesList;
            setupRecyclerFixed();
        }
        private void AutoRefresh()
        {
            fixedList.Clear();
            fixedAdapter.NotifyDataSetChanged();
            Retrieve_fixed();

        }

        private void Business_Click(object sender, EventArgs e)
        {
            txtCaption.Text = "business accounts";
            standardrecycler.Visibility = ViewStates.Gone;
            bussinesrecycler.Visibility = ViewStates.Visible;

            try
            {
                dialogBuilder = new Android.App.AlertDialog.Builder(Activity);
                LayoutInflater inflater = LayoutInflater.From(Activity);
                View dialogView = inflater.Inflate(Resource.Layout.chooseAccount, null);
                electrician = dialogView.FindViewById<ImageView>(Resource.Id.electrician);
                plumber = dialogView.FindViewById<ImageView>(Resource.Id.plumber);
                carpenter = dialogView.FindViewById<ImageView>(Resource.Id.carpenter);
                builder = dialogView.FindViewById<ImageView>(Resource.Id.builder);
                electrician.Click += Electrician_Click;
                plumber.Click += Plumber_Click;
                carpenter.Click += Carpenter_Click;
                builder.Click += Builder_Click;
                dialogBuilder.SetView(dialogView);
                dialogBuilder.SetCancelable(true);
                dialog = dialogBuilder.Create();
                dialog.Show();

            }
            catch (System.Exception)
            {

            }
        }

        private void Builder_Click(object sender, EventArgs e)
        {
            Retrieve_dispose("Architecture");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }

        private void Carpenter_Click(object sender, EventArgs e)
        {

            Retrieve_dispose("Carpenter");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }

        private void Plumber_Click(object sender, EventArgs e)
        {

            Retrieve_dispose("Plumber");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }

        private void Electrician_Click(object sender, EventArgs e)
        {

            Retrieve_dispose("Electrician");
            txtCaption.Text = "";
            if (disposed_Adapter != null)
            {
                disposeList.Clear();
                disposed_Adapter.NotifyDataSetChanged();
            }
            dialog.Dismiss();
        }
        private void setUpRecyclerDisposal()
        {
            LinearLayoutManager linearLayoutManagerDisposal = new LinearLayoutManager(Activity);
            disposed_Adapter = new QuickServiceAdmin.ExtraAdapter.disposedAdapter(disposeList);
            bussinesrecycler.SetLayoutManager(linearLayoutManagerDisposal);
            bussinesrecycler.SetAdapter(disposed_Adapter);
            disposed_Adapter.ItemClick += Disposed_Adapter_ItemClick;


        }

        private void Disposed_Adapter_ItemClick(object sender, ExtraAdapter.disposedAdapterClickEventArgs e)
        {
            Intent u = new Intent(Activity, typeof(BusinessRequests));
            u.PutExtra("BussKey", disposeList[e.Position].id_dsposed);
            StartActivity(u);
        }

        private void Retrieve_dispose(string n)
        {
            dispData.RetriveDispose(n);
            dispData.RetrivedMessages += DispData_RetrivedMessages;
        }
        private void DispData_RetrivedMessages(object sender, DisposalEvent.RetrieveMessageEventHandeler e)
        {
            disposeList = e.messagesList;
            setUpRecyclerDisposal();

        }
    }
}