﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.Helpers;
using Firebase.Database;

namespace QuickServiceAdmin.EventListeners
{
    public class DisposalEvent: Java.Lang.Object, IValueEventListener
    {
        private List<Disosal_DataModel> messages_Stuff = new List<Disosal_DataModel>();
        public event EventHandler<RetrieveMessageEventHandeler> RetrivedMessages;
        public class RetrieveMessageEventHandeler : EventArgs
        {
            public List<Disosal_DataModel> messagesList { get; set; }
        }
        public void OnCancelled(DatabaseError error)
        {

        }
        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot.Value != null)
            {
                messages_Stuff.Clear();
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                foreach (DataSnapshot data in child)
                {
                    Disosal_DataModel meetStuffClass = new Disosal_DataModel
                    {
                        id_dsposed = data.Key,
                        comp_dis = data.Child("company").Value.ToString(),
                        pass_dis = data.Child("passwordCode").Value.ToString(),
                        proff_dis = data.Child("profession").Value.ToString(),
                        email_dis= data.Child("compMail").Value.ToString(),
                        phon_dis= data.Child("contacts").Value.ToString(),
                        date_dis= data.Child("DateRegistered").Value.ToString(),
                        reg_dis = data.Child("regno").Value.ToString()


                    };
                    messages_Stuff.Add(meetStuffClass);
                }
                RetrivedMessages.Invoke(this, new RetrieveMessageEventHandeler
                {
                    messagesList = messages_Stuff
                });
            }
        }
        public void RetriveDispose(string account)
        {
            DatabaseReference dref = firebase_Helper.GetDatabase().GetReference(account);
            dref.AddValueEventListener(this);
        }
    }
}