﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using QuickServiceAdmin.Helpers;
using QuickServiceAdmin.DataModels;


namespace QuickServiceAdmin.EventListeners
{
    public class adminVidData : Java.Lang.Object, IValueEventListener
    {
        private List<videoDataModel> messages = new List<videoDataModel>();
        public event EventHandler<RetrieveMessageEventHandeler> RetrivedMessages;
        public class RetrieveMessageEventHandeler : EventArgs
        {
            public List<videoDataModel> messagesList { get; set; }

        }
        public void OnCancelled(DatabaseError error)
        {

        }
        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot.Value != null)
            {
                messages.Clear();
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                foreach (DataSnapshot data in child)
                {
                    videoDataModel messageClass = new videoDataModel
                    {
                        IDvid = data.Key,
                        LinkVideo = data.Child("video_link").Value.ToString(),
                        nameperson = data.Child("name").Value.ToString(),
                        Time = data.Child("datePosted").Value.ToString(),
                        caption = data.Child("caption").Value.ToString(),
                        userKey= data.Child("ID").Value.ToString()

                    };
                    messages.Add(messageClass);
                }
                RetrivedMessages.Invoke(this, new RetrieveMessageEventHandeler { messagesList = messages });

            }
        }
        public void RetriveMessage()
        {
            firebase_Helper.GetDatabase().GetReference("BusinessAdverts").AddValueEventListener(this);
        }

    }
}