﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.Helpers;
using Firebase.Database;

namespace QuickServiceAdmin.EventListeners
{
    public class EventRequests : Java.Lang.Object, IValueEventListener
    {
        private List<ReqestsDataModel> messages = new List<ReqestsDataModel>();

        public event EventHandler<RetrieveMessageEventHandeler> RetrivedMessages;

        public void OnCancelled(DatabaseError error)
        {
            
        }

        public void OnDataChange(DataSnapshot snapshot)
        {
            try
            {
                if (snapshot.Value != null)
                {
                    messages.Clear();
                    var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                    foreach (DataSnapshot data in child)
                    {

                        ReqestsDataModel messageClass = new ReqestsDataModel
                        {

                            R_id = data.Key,
                            CompanyName = data.Child("CompanyName").Value.ToString(),
                            EmailComp = data.Child("CompanyEmail").Value.ToString(),
                            Regno = data.Child("RgNo").Value.ToString(),
                            status = data.Child("AprovedStatus").Value.ToString(),
                            specialComp = data.Child("Specilization").Value.ToString(),
                            dateDone= data.Child("DateRequested").Value.ToString(),
                            invoiceLink= data.Child("invoiceLink").Value.ToString(),
                            custPhone = data.Child("contacts").Value.ToString(),


                        };
                        messages.Add(messageClass);
                    }
                    RetrivedMessages.Invoke(this, new RetrieveMessageEventHandeler { messagesList = messages });
                }

            }
            catch (Exception)
            {

            }
        }

        public void Retriverequests(string custkey)
        {
            DatabaseReference dref = firebase_Helper.GetDatabase().GetReference("Requests").Child(custkey);
            dref.AddValueEventListener(this);
          
        }
        public class RetrieveMessageEventHandeler : EventArgs
        {
            public List<ReqestsDataModel> messagesList { get; set; }
        }

    }
}