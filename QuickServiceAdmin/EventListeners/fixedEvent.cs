﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.Helpers;

namespace QuickServiceAdmin.EventListeners
{
    public class fixedEvent: Java.Lang.Object, IValueEventListener
    {
        private List<fixed_DataModel> messages_Stuff = new List<fixed_DataModel>();
        public event EventHandler<RetrieveMessageEventHandeler> RetrivedMessages;
        public class RetrieveMessageEventHandeler : EventArgs
        {
            public List<fixed_DataModel> messagesList { get; set; }
        }
        public void OnCancelled(DatabaseError error)
        {

        }
        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot.Value != null)
            {
                messages_Stuff.Clear();
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                foreach (DataSnapshot data in child)
                {
                    fixed_DataModel meetStuffClass = new fixed_DataModel
                    {

                        Id_ = data.Key,
                        f_name_ = data.Child("Name").Value.ToString(),
                        f_contacts_ = data.Child("contacts").Value.ToString(),
                        f_passcode_ = data.Child("passwordCode").Value.ToString(),
                        f_email= data.Child("Email").Value.ToString()

                    };
                    messages_Stuff.Add(meetStuffClass);
                }
                RetrivedMessages.Invoke(this, new RetrieveMessageEventHandeler
                {
                    messagesList = messages_Stuff
                });
            }
        }
        public void RetriveFixed()
        {
            DatabaseReference dref = firebase_Helper.GetDatabase().GetReference("standardUsers");
            dref.AddValueEventListener(this);
        }
    }
}