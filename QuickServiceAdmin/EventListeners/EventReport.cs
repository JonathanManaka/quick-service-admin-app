﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Firebase.Database;
using QuickServiceAdmin.DataModels;
using QuickServiceAdmin.Helpers;

namespace QuickServiceAdmin.EventListeners
{
    public class EventReport : Java.Lang.Object, IValueEventListener
    {
        private List<RequestReportDataModel> messages_Stuff = new List<RequestReportDataModel>();
        public event EventHandler<RetrieveMessageEventHandeler> RetrivedMessages;

        public void OnCancelled(DatabaseError error)
        {
            throw new NotImplementedException();
        }
        public void RetriveReport(string x)
        {
            DatabaseReference dref = firebase_Helper.GetDatabase().GetReference("BusinessCopyRequests").Child(x);
            dref.AddValueEventListener(this);
        }
        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot.Value != null)
            {
                messages_Stuff.Clear();
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                foreach (DataSnapshot data in child)
                {
                    RequestReportDataModel meetStuffClass = new RequestReportDataModel
                    {

                        _id = data.Key,
                        name = data.Child("NameClient").Value.ToString(),
                        status = data.Child("AprovedStatus").Value.ToString(),
                        company = data.Child("CompanyName").Value.ToString(),
                        requstDate = data.Child("DateRequested").Value.ToString(),
                        invoice = data.Child("invoiceLink").Value.ToString(),
                        phone = data.Child("contacts").Value.ToString()

                    };
                    messages_Stuff.Add(meetStuffClass);
                }
                RetrivedMessages.Invoke(this, new RetrieveMessageEventHandeler
                {
                    messagesList = messages_Stuff
                });
            }
        }

        public class RetrieveMessageEventHandeler : EventArgs
        {
            public List<RequestReportDataModel> messagesList { get; set; }
        }

    }
}